// [SECTION] Objects

//  AN OBJECT IS a data type that is used to represent a real world object.

// It is a collection of related data and/or functionalities.
// information is stored in object represented in "key:value" pair

// key - > property of the OBJECT
// value -> actual data to be stored.

// different data type may be stored in an object's property creating a complex data structures.

// two ways in creating object in JavaScript
// 1. object literal notation
// 2. object constructor notation

// object literal notation
// Creating objects using initializer/literal notation.

//syntax:
// let objectName = {
// keyA: valueA,
// keyB: valueB,
//}

let cellphone = {
  name: "Nokia 3210",
  manufactureDate: 1999,

};

console.log("Result from creating objects using initializer/Literal notation: ");
console.log(cellphone);

let cellphone2 = {
  name: "Iphone 13",
  manufactureDate: 2021,
}
console.log(cellphone2);


let cellphone3 = {
  name: "Xiaomi 11t Pro",
  manufactureDate: 2021,
}
console.log(cellphone3);





//Objcet Constructor notation

// Creating objects using a constructor function.
// creates a reusable function to create several objects that have the same data structure.


/*
    -syntax
      function objectName(keyA, KeyB) {
      this.KeyA = keyA;
      ThisKeyB = keyB;
    }


//"This" keyword refers to the properties within the object.
  // itallows the assignment of new object's properties by associating them with values receive from the constructor functions's parameter.

  */

function Laptop(name, manufactureDate) {
  this.name = name;
  this.manufactureDate = manufactureDate;
}

//create an instance object using the Laptop Constructor
let laptop = new Laptop("Lenovo", 2008);

console.log("Result from creating objects using ojbect constructor: ");
console.log(laptop);

let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating objects using object constructor: ");
console.log(myLaptop);

//Returns "undefined" without the "new" operator because the Laptop function does not have a return statement.
let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result from creating objects using object constructor: ");
console.log(oldLaptop);



//create new empty Objects

let computer = {} // object literal notation
let myComputer = new Object(); // object constructor notation -> instantiate
//[SECTION] Accessing object properties
//using dot notation

console.log("Result from dot notation: " + myLaptop.name);

// using square bracket notation
console.log("Result from square bracket notation: " + myLaptop["name"]);



//accessing array of objects
//accessing objeect properties using the square bracket notation and array indexes can cause confusion.


let arrayObj = [laptop, myLaptop];
console.log(arrayObj[0]);
console.log(arrayObj[0].name);


//SECTION INITIALIZING/ADDING/DELETING REASSIGNNING OBJECT PROPERTIES

let car = {}
console.log("Current value of car object: ")
console.log(car);

//Initializing/adding object PROPERTIES
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation: ");
console.log(car);

// initialize/adding object properties using bracket notation
// car["manufacture date"] = 2019;
// console.log(car["manufacture date"]);
console.log("Result from adding a properties using square bracket notation: ");
car["manufacture date"] = 2019;
console.log(car)


//Deleting object PROPERTIES

delete car["manufacture date"];
console.log("result from deleteing properties: ");
console.log(car)

car["manufactureDate"] = 2019
console.log(car);


//reassigning object property values
car.name = "Toyota Vios";
console.log("Result from reassigning property values: ");
console.log(car);

// [SECTION] OBJECT method
// A method is function which is a property of an object.

let person = {
  name: "John",
  talk: function() {
    console.log("Hello my name is " + this.name);
  }
}

console.log(person);
console.log("Result from object: ");
person.talk();

person.walk = function(steps) {
  console.log(this.name + " walked " + steps + " steps forward");

}
console.log(person);
person.walk(50);

//Methods are useful fro creating reusable functions that perform tasks related to objects.

let friend = {
  firstName: "Joe",
  lastName: "Smith",
  address: {
    city: "Austin",
    country: "Texas"
  },
  emails: ["joe@mail.com", "joesmith@mail.xyz"],
  introduce: function() {
    console.log("Hello my name is " + this.firstName + " " + this.lastName + " I live in " + this.address.country);
  }
}

friend.introduce();

//[SECTION] Real world Application of Objects

/*
      Scenario
      1. We would like to create a game that would have a several pokemon interact with each other.
      2. Every pokemon would have the same set of stats, properties and function.


      Stats:
      name
      level
      health = level * 2
      attack = level


*/


// Create an object constructor to lessen the process in creating the pokemon.

function Pokemon(name, level) {
  //PROPERTIES
  this.name = name;
  this.level = level;
  this.health = (level * 2);
  this.attack = level;


  //Mini Activity
  //if target health is less than or equal to 0 we will invoke the faint() method, otherwise printout the pokemon's new targe health

  this.faint = function() {
    console.log(this.name + " fainted.");
  }
  //Methods
  this.tackle = function(target) {
    console.log(this.name + " tackled " + target.name)
    //target.health = target.health - this.attack
    // console.log("targetPokemon's health is now  reduced to targePokemonHealth");
    //if target health less than or equal to 0;
    target.health -= this.attack
    target.health = target.health - this.attack * 2;
    if (target.health <= 0) {
      //  console.log(target.name+ " health is now reduced to "+ "ashes")
      target.faint();

    } else {
      console.log(target.health - (this.attack * 2) + " remaining HP");
    }
  }

}
let pikachu = new Pokemon("Pikachu", 99);
console.log(pikachu);

let rattata = new Pokemon("Rattata", 5);
console.log(rattata);







//
